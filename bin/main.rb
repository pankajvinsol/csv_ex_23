require_relative '../lib/employee_csv_reader.rb'

current_dir = File.dirname(__FILE__) + "/"
input_file_url = current_dir + "data.csv"
output_file_url = current_dir + "output.txt"
employee_group = EmployeeCsvReader.group_by_designation(input_file_url)
EmployeeCsvReader.write_to_file(employee_group, output_file_url)

