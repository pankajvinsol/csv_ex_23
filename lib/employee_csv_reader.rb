require 'csv'

class EmployeeCsvReader
  def self.group_by_designation(input_file)
    employee_group = Hash.new { |hash, key| hash[key] = [] }
    CSV.foreach(input_file, headers: true, col_sep: ", ") do |row|
      employee_group[row["Designation"]].push({name: row["Name"], empid: row["EmpId"]})
    end
    employee_group.each { |key, value| employee_group[key] = value.sort_by { |employee| employee[:empid] }}
    employee_group.sort.to_h
  end

  def self.write_to_file(employee_group, output_file)
    output_file = File.new(output_file, 'w')
    employee_group.each do |key, value|
      output_file << (employee_group[key].length > 1 ? key + "s\n" : key + "\n")
      value.each { |employee| output_file << "#{ employee[:name] } (EmpId: #{ employee[:empid] })\n" }
      output_file << "\n"
    end
  end

end

